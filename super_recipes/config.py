import os
from pathlib import Path

basedir = Path(__file__).parent.absolute()
webpath = Path("/")  # This path should only be used for web requests
SQLALCHEMY_DATABASE_URI = "sqlite:///" + str(basedir / "app.db")
SQLALCHEMY_TRACK_MODIFICATIONS = True

WTF_CSRF_ENABLED = True
SECRET_KEY = "xFC8rWAW"

# Flask Security
SECURITY_PASSWORD_SALT = os.environ.get(
    "SECURITY_PASSWORD_SALT", "30105961651786192325365376438291656451")

IMAGES_DIR = basedir / "static" / "images"
IMAGES_URL = webpath / "static" / "images"
