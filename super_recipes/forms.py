from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, TextAreaField, FileField
from wtforms.validators import DataRequired


class RecipeForm(FlaskForm):
    name = StringField("name", validators=[DataRequired()])
    description = TextAreaField("description", validators=[DataRequired()])
    serves = IntegerField("serves", validators=[DataRequired()])
    cooking_time = IntegerField("cooking_time", validators=[DataRequired()])
    method = TextAreaField("method", validators=[DataRequired()])
    img_url = FileField("img_url")


class IngredientForm(FlaskForm):
    name = StringField("name", validators=[DataRequired()])
    description = TextAreaField("description", validators=[DataRequired()])
    img_url = FileField("img_url")


class UnitForm(FlaskForm):
    name = StringField("name", validators=[DataRequired()])
    short_name = StringField("short_name", validators=[DataRequired()])
