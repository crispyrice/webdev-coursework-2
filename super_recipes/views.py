from . import app, db, config
from .models import Recipe, Ingredient, Unit, RecipeIngredient
from .forms import RecipeForm, IngredientForm, UnitForm
from flask import jsonify, redirect
from flask.templating import render_template
from flask_security import auth_required
from werkzeug.utils import secure_filename

title = "Super Recipes!"
navbar_links = ["Meal of the Day", "Lunch Ideas"]


@app.route("/")
def index() -> str:
    return render_template("home.html",
                           title=title,
                           navbar_links=navbar_links)


@app.route("/recipe/create", methods=["GET", "POST"])
@auth_required()
def create_recipe() -> str:
    form = RecipeForm()
    if form.validate_on_submit():
        recipe = Recipe()
        recipe.name = form.name.data
        recipe.description = form.description.data
        recipe.serves = form.serves.data
        recipe.cooking_time = form.cooking_time.data
        recipe.method = form.method.data
        if form.img_url.data:
            filename = secure_filename(form.img_url.data.filename)
            saved_url = config.IMAGES_DIR / filename
            web_url = config.IMAGES_URL / filename
            form.img_url.data.save(str(saved_url))
            recipe.img_url = str(web_url)

        db.session.add(recipe)
        db.session.commit()
        return jsonify({
            "created": True,
            "info": {
                "recipe_id": recipe.id
            }
        })
    return render_template("recipe/create.html",
                           title=title,
                           page_name="New Recipe",
                           navbar_links=navbar_links,
                           form=form,
                           form_name="recipe")


@app.route("/recipe/update/<recipe_id>", methods=["GET", "POST"])
@auth_required()
def update_recipe(recipe_id: int) -> str:
    # Get the recipe to update
    recipe_to_update = Recipe.query.get(recipe_id)
    form = RecipeForm(obj=recipe_to_update)
    if form.validate_on_submit():
        recipe_to_update.name = form.name.data
        recipe_to_update.description = form.description.data
        recipe_to_update.serves = form.serves.data
        recipe_to_update.cooking_time = form.cooking_time.data
        recipe_to_update.method = form.method.data
        if form.img_url.data.filename:
            filename = secure_filename(form.img_url.data.filename)
            saved_url = config.IMAGES_DIR / filename
            web_url = config.IMAGES_URL / filename
            form.img_url.data.save(str(saved_url))
            recipe_to_update.img_url = str(web_url)

        db.session.commit()
        return jsonify({
            "updated": True,
            "info": {
                "recipe_id": recipe_to_update.id
            }
        })
    return render_template("recipe/update.html",
                           title=title,
                           page_name="Edit Recipe",
                           navbar_links=navbar_links,
                           form=form,
                           form_name="recipe",
                           ingredients=[ri for ri in RecipeIngredient.query
                                        .filter_by(recipe_id=recipe_id)])


@app.route("/recipe/delete/<recipe_id>")
@auth_required()
def delete_recipe(recipe_id: int) -> str:
    recipe_to_delete = Recipe.query.get(recipe_id)
    # Remove all RecipeIngredients first
    for ri in recipe_to_delete.recipe_ingredients:
        db.session.delete(ri)
    if recipe_to_delete.img_url:
        (config.basedir / recipe_to_delete.img_url[1:]).unlink()
    db.session.delete(recipe_to_delete)
    db.session.commit()
    return redirect("/")


@app.route("/recipe/<recipe_id>")
def view_recipe(recipe_id: int) -> str:
    recipe = Recipe.query.get(recipe_id)
    if not recipe:
        return render_template("404.html",
                               title=title,
                               page_name="Not Found",
                               navbar_links=navbar_links)
    return render_template("recipe/view.html",
                           title=title,
                           page_name=recipe.name,
                           navbar_links=navbar_links,
                           recipe=recipe,
                           method_markdown=recipe.method)


@app.route("/ingredient/create", methods=["GET", "POST"])
@auth_required()
def create_ingredient() -> str:
    form = IngredientForm()
    if form.validate_on_submit():
        ingredient = Ingredient()
        ingredient.name = form.name.data
        ingredient.description = form.description.data
        if form.img_url.data:
            filename = secure_filename(form.img_url.data.filename)
            saved_url = config.IMAGES_DIR / filename
            web_url = config.IMAGES_URL / filename
            form.img_url.data.save(str(saved_url))
            ingredient.img_url = str(web_url)

        db.session.add(ingredient)
        db.session.commit()
        return redirect(f"/ingredient/{ingredient.id}")
    return render_template("ingredient/create.html",
                           title=title,
                           page_name="New Ingredient",
                           navbar_links=navbar_links,
                           form=form,
                           form_name="ingredient")


@app.route("/ingredient/<ingredient_id>")
def view_ingredient(ingredient_id: int) -> str:
    ingredient = Ingredient.query.get(ingredient_id)
    if not ingredient:
        return render_template("404.html",
                               title=title,
                               page_name="Not Found",
                               navbar_links=navbar_links)
    recipes = [ri.recipe for ri in ingredient.recipe_ingredients]
    return render_template("ingredient/view.html",
                           title=title,
                           page_name=ingredient.name,
                           navbar_links=navbar_links,
                           ingredient=ingredient,
                           recipes=recipes)


@app.route("/unit/create", methods=["GET", "POST"])
@auth_required()
def create_unit() -> str:
    form = UnitForm()
    if form.validate_on_submit():
        unit = Unit()
        unit.name = form.name.data
        unit.short_name = form.short_name.data
        db.session.add(unit)
        db.session.commit()
        return redirect(f"/unit/{unit.id}")
    return render_template("unit/create.html",
                           title=title,
                           page_name="New Unit",
                           navbar_links=navbar_links,
                           form=form)


@app.route("/unit/<unit_id>")
def view_unit(unit_id: int) -> str:
    unit = Unit.query.get(unit_id)
    if not unit:
        return render_template("404.html",
                               title=title,
                               page_name="Not Found",
                               navbar_links=navbar_links)
    return render_template("unit/view.html",
                           title=title,
                           page_name=unit.name,
                           navbar_links=navbar_links,
                           unit=unit)
